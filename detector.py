import os

import numpy as np
from astropy.io import fits
from skimage.filters import threshold_otsu
from skimage.segmentation import flood_fill
from skimage.transform import hough_line, hough_line_peaks

from visualise import figure, plot_image, plot_h_image, plot_hough_image, plot_line_candidates, display_plots
from tests import BaseTest, SecondTest, NoCRTest


def read_image(filepath, n=0, offset=None):
    fits_file = fits.open(os.path.join(filepath))
    data = fits_file[n].data
    fits_file.close()

    if offset:
        return data[offset:-offset, offset:-offset]

    return data


def otsu_filter(image):
    t_hold = threshold_otsu(image[image > 0])
    image[image < t_hold] = 0

    return image


def hough_transform(image, n, regions, pad):
    # Perform a classic straight-line Hough transform
    # todo - to be replace by LM's implementation

    tested_angles = np.zeros(shape=n * 2 * len(regions))
    for i, r in enumerate(regions):
        tested_angles[i * n * 2: i * n * 2 + n * 2] = np.linspace(r - pad, r + pad, n * 2)

    h, theta, d = hough_line(image, theta=tested_angles)

    return h, theta, d


def line_detector(image, h, theta, d, star_pos, error=5):
    candidates = []
    origin = np.array((0, image.shape[1]))

    for _, angle, dist in zip(*hough_line_peaks(h, theta, d)):

        y0, y1 = (dist - origin * np.cos(angle)) / np.sin(angle)
        p1 = np.array([y0, origin[0]])
        p2 = np.array([y1, origin[1]])

        d = np.linalg.norm(np.cross(p2 - p1, p1 - star_pos)) / np.linalg.norm(p2 - p1)

        if abs(d) > error:
            continue

        print(f"Spike found: {np.rad2deg(angle): 0.2f} deg")
        candidates.append([origin, (y0, y1)])

    print(f"Found {len(candidates)} spikes")
    return candidates


def est_star_radius(image, centre):
    x, y = centre

    xrange = range(0, x, 1)
    yrange = [y] * len(xrange)

    power = image[yrange, xrange]

    i_max_power = np.argmax(power)
    # determine the index at which the power is lower than the median power.
    index = np.argwhere(power[:i_max_power] < np.median(power))[-1][0]

    return x - index + (x - i_max_power)


def get_candidates(filepath, star_positions, visualise=True):
    spikes = []
    print(f"\nProcessing image at {filepath}")
    org_image = read_image(filepath, 1, offset=0)
    # mask the star centre
    for star_coor in star_positions:
        star_x, star_y = star_coor

        # create a box around x,y of the given star coordinate.
        b_box = org_image[star_y - offset_bbox[0]:star_y + offset_bbox[0],
                star_x - offset_bbox[1]:star_x + offset_bbox[1]]

        image = flood_fill(b_box, offset_bbox, vmin, tolerance=10000, in_place=True)

        # mask the center of the star.
        radius = est_star_radius(image, offset_bbox)
        radsq = radius * radius
        y = np.arange(np.shape(image)[0], dtype=np.int64)
        x = np.arange(np.shape(image)[1], dtype=np.int64)
        xv, yv = np.meshgrid(x - offset_bbox[0], y - offset_bbox[1])
        rsq = xv * xv + yv * yv
        image[rsq < radsq] = vmin

    for star_coor in star_positions:
        print(f"Spike candidates for star at x={star_coor[0]}, y={star_coor[1]}")
        fig, (ax1, ax2, ax3, ax4) = figure(4)

        # Deep copy of the image
        image = org_image.copy()
        star_x, star_y = star_coor

        # create a box around x,y of the given star coordinate.
        b_box = image[star_y - offset_bbox[0]:star_y + offset_bbox[0], star_x - offset_bbox[1]:star_x + offset_bbox[1]]
        image = b_box

        if visualise:
            plot_image(ax1, b_box, labels=('Input image', 'x', 'y'), v_limits=(vmin, vmax))

        # Simple background noise removal filter - to be replaced
        image[image < np.median(image)] = 0

        if visualise:
            plot_image(ax2, image, labels=('De-noised image', 'x', 'y'), v_limits=(vmin, vmax))

        h, theta, d = hough_transform(image, N, regions, pad)

        if visualise:
            plot_h_image(ax3, np.log(1 + h), labels=('Hough transform', 'Angles (degrees)', 'Distance (pixels)'),
                         extent=[theta[0], theta[-1], d[0], d[-1]])

            # plot_h_image(
            #     np.log(1 + h), labels=('Hough transform', 'Angles (degrees)', 'Distance (pixels)'),
            #     extent=[theta[0], theta[-1], d[0], d[-1]]            )

            # plot_hough_image(np.log(1 + h), ('Hough transform', 'Angles (degrees)', 'Distance (pixels)'), d, theta)

        # The star is assumed to be at the centre of the image
        star_pos = offset_bbox

        candidates = line_detector(image, h, theta, d, offset_bbox, error)

        if visualise:
            plot_line_candidates(ax4, image, labels=('Detected Lines', 'x', 'y'), v_limits=(vmin, vmax),
                                 line_candidates=candidates)

        if visualise:
            fig.savefig(f'diffraction_spike_{star_x:2d}_{star_y:2d}.png')

        spikes.append(candidates)

    if visualise:
        display_plots()

    return spikes


if __name__ == '__main__':
    VISUALISATION = True
    # regions_deg = [-79.24, -17.017, 39.65]
    regions = [-1.383, -0.297, 0.692]  # predicted slope angle regions (in rad) of the diffraction spikes

    pad = 0.01  # spread around the predicted slope angle (in rad.)
    N = 5  # number of test angles per predicted slope angle
    error = 10
    vmin, vmax = (1999, 2055)  # clipping values to use in visualisation

    # todo - the size of the bounding box around the star needs to be determined automatically
    offset_bbox = (250, 250)

    tests = [BaseTest,
             SecondTest,
             NoCRTest]

    for t in tests:
        spikes = get_candidates(filepath=t.image_filepath, star_positions=t.stars_position, visualise=VISUALISATION)
