import astropy.io.fits as pf
from matplotlib import pyplot as plt
import numpy as np
import os


def plot_gal_position(catalogue, title, n_gal=50):
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))
    data = catalogue[1].data[:n_gal]

    ax1.plot(data['RA'], data['DEC'], 'b.', label='ra, dec')
    ax1.plot(data['RA_MAG'], data['DEC_MAG'], 'r.', label='ra_mag, dec_mag')

    ax1.set_title(title)
    ax1.set_xlabel('RA (deg)')
    ax1.set_ylabel('Dec. (deg)')
    ax1.legend()


def plot_magnitude(gaia_vis_data, besancon_vis_data):
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))
    num_bins = 50
    n, bins, patches = ax1.hist(gaia_vis_data, num_bins, facecolor='red', alpha=0.8, label='GAIA')
    n, bins, patches = ax1.hist(besancon_vis_data, num_bins, facecolor='blue', alpha=0.8, label='Besancon')

    ax1.set_xlabel('VIS Mag.')
    ax1.set_ylabel('Count')
    plt.legend()


def plot_sed_index(data1, data2, data3):
    x = data1['SED_TEMPLATE']
    num_bins = 25
    n, bins, patches = plt.hist(x, num_bins, facecolor='blue', alpha=0.5, label='gaia')

    x = data2['INDEX_SED']
    num_bins = 25
    n, bins, patches = plt.hist(x, num_bins, facecolor='red', alpha=0.5, label='Besancon')

    x = data3['SED_TEMPLATE']
    num_bins = 25
    n, bins, patches = plt.hist(x, num_bins, facecolor='green', alpha=0.5, label='Original Elvis')

    plt.legend()
    plt.show()


def flux2mag(flux):
    zero_point = 3631.
    return -2.5 * np.log10(flux / zero_point)


def plot_magnitude_dist(flux):
    num_bins = 250
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))

    mag = flux2mag(flux)

    n, bins, patches = plt.hist(mag, num_bins, facecolor='green', alpha=0.5, label='tu_fnu_vis_mag')


def show_cat_columns(catalogue):
    print("Catalog columns:")
    for c in catalogue[1].data.columns:
        print(c.name)

    return None


def subset_gal_catalogue(cat_filepath, flux_key='tu_fnu_vis_mag', mag_clip=23):
    new_cat_filepath = get_new_subset_filename(cat_filepath, mag_clip)
    with pf.open(cat_filepath) as hdu_list:
        hdu = hdu_list[1]

        n_org = len(hdu_list[1].data)

        flux = flux2mag(hdu_list[1].data[flux_key])
        hdu.data = hdu_list[1].data[flux < mag_clip]
        hdu_list.writeto(new_cat_filepath)

        print(f"Wrote {len(hdu.data)} galaxies (from {n_org} galaxies) to {new_cat_filepath}")


def get_new_subset_filename(org_filepath, mag_clip):
    basename = os.path.basename(org_filepath)
    filename = basename.split(".")[0]
    filepath = os.path.join(os.path.dirname(org_filepath), filename)

    return f"{filepath}_MAG_{mag_clip:1d}.fits"


if __name__ == '__main__':
    ROOT = "/home/denis/Development/ELViS/work_dir/"
    nep_cat_filepath = ROOT + "ppos/EUC_SIM_TUGALCAT_NEPLC.fits"
    org_cat_filepath = "./resources/EUC_SIM_TUGALCAT.fits"

    nep_cat = pf.open(nep_cat_filepath, mode='denywrite')
    org_cat = pf.open(org_cat_filepath, mode='denywrite')

    show_cat_columns(catalogue=nep_cat)

    # plot_gal_position(nep_cat, "NEP (rotated) catalogue", n_gal=50)
    # plot_gal_position(org_cat, "Original catalogue", n_gal=50)

    # plot the magnitude distribution of the catalog
    # plot_magnitude_dist(nep_cat[1].data['tu_fnu_vis_mag'])

    subset_gal_catalogue(nep_cat_filepath, flux_key='tu_fnu_vis_mag', mag_clip=20)

    plt.show()
