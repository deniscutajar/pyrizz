class Test:
    stars_position = None
    image_filepath = None


class BaseTest(Test):
    stars_position = ((1513, 1811), (472, 1872), (590, 1899))
    image_filepath = "resources/test_image.fits"


class NoCRTest(Test):
    stars_position = ((1513, 1811), (472, 1872), (590, 1899))
    image_filepath = "resources/VIS_sim_no_CR.fits"


class SecondTest(Test):
    stars_position = ((1194, 555), (804, 700))
    image_filepath = "resources/VIS_sim_2.fits"
