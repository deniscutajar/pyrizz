# standard imports
import os
import sys

# imports for handling VIS throughput from PSF Toolkit
from SHE_PSFToolkit.weights.instrument import InstrumentResponse
from SHE_PSFToolkit.weights.sed import SEDResponse_Pickles
from SHE_PSFToolkit.weights.weighting import Weighting

# Toolkit needs initialised for Instrument Response.
# Note that this will need to point to the location of the default config on your system. 
from SHE_PSFToolkit.settings.share import initialise_settings

# cfgfile = "/Users/miller/Euclid/Oxford/OpticalPSF/PSF_Toolkit/default_model.config"
cfgfile = None
if cfgfile is None or os.path.exists(cfgfile):
    initialise_settings(cfgfile)
else:
    print ('PSF Toolkit config file ',cfgfile,' does not exist ')
    sys.exit(1)

# imports for handling HST/ACS photometry from stsynphot and synphot
# these can be imported from AstroConda
# see https://stsynphot.readthedocs.io/en/latest/
# see https://synphot.readthedocs.io/en/latest/index.html
from synphot import SourceSpectrum, Observation, SpectralElement, units
from synphot.models import ConstFlux1D, Empirical1D

def VIS_response():
    """
    get the VIS throughput from the PSF Toolkit
    convert to synphot format
    """
    instrument = InstrumentResponse()
    VISresponse = instrument.get_total_throughput(None,None)
    # convert VISresponse wavelengths to Angstrom
    VISresponse[:,0] *= 10.
    # create a synphot version of the VIS response
    bp_vis = SpectralElement(Empirical1D, points=VISresponse[:,0],lookup_table=VISresponse[:,1])
    return bp_vis, VISresponse[:,0]

