# PyRizz

A small repository for detection of diffraction spike in astronomical images.

## Installation

```bash
pip install -r requirements.txt
```
 
## How to Run

Source the virtual environment
```bash
source ~/venv/bin/activate
```

Run the detector
```bash
python detector.py
```