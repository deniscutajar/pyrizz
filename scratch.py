import os

import numpy as np
from astropy.io import fits
from skimage.filters import threshold_otsu
from skimage.transform import hough_line, hough_line_peaks

from visualise import figure, plot_image, plot_line_candidates, display_plots
from skimage.segmentation import flood, flood_fill
import matplotlib.pyplot as plt
from scipy import ndimage

def read_image(filepath, n=0, offset=None):
    fits_file = fits.open(os.path.join(filepath))
    data = fits_file[n].data
    fits_file.close()

    if offset:
        return data[offset:-offset, offset:-offset]

    return data



if __name__ == '__main__':
    regions = [-1.383, -0.297, 0.692]  # predicted slope angle regions (in rad) of the diffraction spikes
    pad = 0.01  # spread around the predicted slope angle
    N = 50  # number of test angles per predicted slope angle
    error = 5

    vmin, vmax = (1999, 2055)  # clipping values to use in visualisation

    # image = read_image("resources/VIS_sim.fits", 1, offset=0)
    # template = read_image("resources/extracted_image_3.fits", 0, offset=150)
    image = read_image("resources/test_image.fits", 1, offset=0)

    # detect star candidates
    stars = ((1513, 1811), (472, 1872), (590, 1899))  # "resources/test_image.fits"
    stars = [(1513, 1811)]
    stars = [(472, 1872)]


    fig, (ax1, ax2, ax3, ax4) = figure(4)
    for star_coor in stars:
        star_x, star_y = star_coor

        # todo - needs to be determined automatically
        offset_bbox = (200, 200)

        # create a box around x,y of the given star coordinate.
        b_box = image[star_y - offset_bbox[0]:star_y + offset_bbox[0], star_x - offset_bbox[1]:star_x + offset_bbox[1]]

        img_45 = ndimage.rotate(b_box, 45, reshape=False)


