import os

import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from astropy.visualization import simple_norm
from photutils import CircularAperture
from photutils import find_peaks
from sklearn.cluster import DBSCAN
from skimage import measure
from skimage.filters import threshold_triangle


def save_fits(data, path):
    """
    Save FITS data.
    :param data: FITS data to save, np.array format.
    :param path: Path to where data is to be saved.
    """
    hdu = fits.PrimaryHDU(data)
    hdulist = fits.HDUList([hdu])
    hdulist.writeto(path, overwrite=True)
    return


def read_image(directory, file_name):
    fits_file = fits.open(os.path.join(directory, file_name))
    data = fits_file[1].data
    fits_file.close()

    # return fits_file[1].data[:800, :800]
    return data


def get_clusters(ndx, c_labels):
    # Add cluster labels to the data
    labelled_data = np.append(ndx, np.expand_dims(c_labels, axis=1), axis=1)

    # Cluster mask to remove noise clusters
    de_noised_data = labelled_data[labelled_data[:, 3] > -1]

    de_noised_data = de_noised_data[de_noised_data[:, 3].argsort()]

    # Return the location at which clusters where identified
    cluster_ids = np.unique(de_noised_data[:, 3], return_index=True)

    # Split the data into clusters
    clusters = np.split(de_noised_data, cluster_ids[1])

    # remove empty clusters
    clusters = [x for x in clusters if np.any(x)]

    return clusters

def extract_image(position, pd):

    return

def naive_dbscan(test_image):
    # mean, med, std = sigma_clipped_stats(test_image)
    # test_image -= mean

    local_thresh = threshold_triangle(test_image, nbins=1024)
    local_filter_mask = test_image < local_thresh

    test_image[local_filter_mask] = 0

    noise_est = 0

    db_scan = DBSCAN(eps=30, min_samples=5, algorithm='kd_tree', n_jobs=-1)

    ndx = np.column_stack(np.where(test_image > noise_est))

    ndx = np.append(ndx, np.expand_dims(test_image[test_image > noise_est], axis=1), axis=1)

    try:
        # Perform (2D) clustering on the data and returns cluster labels the points are associated with
        c_labels = db_scan.fit_predict(ndx)
    except ValueError:
        return []

    if len(c_labels) < 1:
        return []

    return get_clusters(ndx, c_labels)

def pad(img, h, w):
    #  in case when you have odd number
    z = np.zeros(shape=(480,480))
    z[25:-25, 25:-25] = img

    return z

#
# clusters = naive_dbscan(data)
#
# print(f"detected {len(clusters)} clusters")
# for c in clusters:
#     ax2.plot(c[:, 1], c[:, 0], '.')
#
# # ax2.imshow(data, origin="lower")
#
# plt.show()

if __name__ == '__main__':

    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2)
    data = read_image("resources", "VIS_sim_no_CR.fits")

    star_pos = 471, 1871
    pd = 215

    extracted_data = data[star_pos[1]-pd:star_pos[1]+pd, star_pos[0]-pd:star_pos[0]+pd]

    padded_data = pad(extracted_data, 25, 25)


    save_fits(padded_data,  "extracted_image_no_cr.fits")
    exit()

    # data[:, 228:248] = 0

    max = np.max(data)
    data[data == max] = 0
    # data[data < 3000] = 0
    # data[data > 15000] = 0

    # ax1.imshow(data, origin="lower")
    # d = np.where(data > 0, 1, 0)
    ax1.imshow(data, origin="lower")

    # Find contours at a constant value of 0.8
    # contours = measure.find_contours(data, 1000)
    #
    # for contour in contours:
    #     ax2.plot(contour[:, 1], contour[:, 0], linewidth=2)

    clusters = naive_dbscan(data)

    for c in clusters:
        if len(c) > 100:
            print(len(c))
            ax2.plot(c[:, 1], c[:, 0], '.')

    plt.show()
    exit()
