import astropy.io.fits as pf
import numpy as np

import swig_sed_library
from Assign_Gaia_SED import readSED, get_mags, get_bandpasses
from av_calc import get_mwext

if __name__ == '__main__':
    SED_CATALOGUE = 'EUC-TEST-SEDLIB-2013-11-14_MODIF.fits'
    OUTPUT_AV_SED_MAP = 'EUC-TEST-SEDLIB-2013-11-14_MODIF_REDDENING.npy'
    OUTPUT_AV_SEDMAG_MAP = 'EUC-TEST-SEDLIB-2013-11-14_MODIF_SEDMAG.npy'
    SCHLEGEL_NORTH_FILEPATH = 'SFD_dust_4096_ngp.fits'  # https://github.com/kbarbary/sfddata
    SCHLEGEL_SOUTH_FILEPATH = 'SFD_dust_4096_sgp.fits'  # https://github.com/kbarbary/sfddata
    AV_start = 0.100
    AV_stop = 0.160
    AV_step = 0.001

    # create the VIS, Gaia bandpasses and zeropoint definitions
    bandlist, zplist, refmag = get_bandpasses()

    wlens, seds, params = readSED(SED_CATALOGUE)
    mwext = get_mwext(SCHLEGEL_NORTH_FILEPATH, SCHLEGEL_SOUTH_FILEPATH)

    avs = np.arange(start=AV_start, stop=AV_stop, step=AV_step)
    av_sed_matrix = np.zeros(shape=(avs.shape[0], seds.shape[0], seds.shape[1]))

    # get the spectrum parameters that were read from the SEDs file
    logg = params.field(0)
    teff = params.field(1)
    feh = params.field(2)

    # select dwarf stars (high gravity), with reasonable feh metallicity, and not too hot from the SED table
    starsel = (logg > 3.5) & (feh > -1) & (teff < 12000)

    av_sed_mags_matrix = np.zeros(shape=(avs.shape[0], len(starsel), 5))

    for i, av in enumerate(avs):
        for j, sed in enumerate(seds):
            s_sed = swig_sed_library.DoubleVector(sed)
            mwext.apply_mw_extinction(s_sed, av)

            av_sed_matrix[i, j, :] = s_sed

        # get the magnitudes in each band of the selected template SEDs
        sed_mags = get_mags(av_sed_matrix[i, starsel, :], wlens, bandlist, zplist, refmag)

        av_sed_mags_matrix[i, starsel, :] = sed_mags
        print(f"Processed AV:{av:2.3f} ({i + 1} of {len(avs)})")

    # Save the AV SED matrix to disk
    np.save(OUTPUT_AV_SED_MAP, av_sed_matrix)

    # Save the AV SED MAG matrix to disk
    np.save(OUTPUT_AV_SEDMAG_MAP, av_sed_mags_matrix)
