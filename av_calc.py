import swig_mw_extinction
import numpy as np
import astropy.units as u
import astropy.io.fits as pf


def setup_wgrid(start, stop, bin):
    """
    Function taken directly from SIMTOOLS

    Create the wavelength grid for the SIMTOOLS spectra generator
    :param start: staring wavelength (astropy quantity)
    :param stop: stop wavelength (astropy quantity)
    :param bin: wavelength bin (astropy quantity)
    :return: numpy array of wavelength in Angstrom
    """

    stop = stop + bin / 2.
    return np.arange(start.to(u.Angstrom).value, stop.to(u.Angstrom).value, bin.to(u.Angstrom).value)


def read_catalog(filepath):
    return pf.open(filepath, mode='update')


def get_mwext(sn_filepath, ss_filepath):
    # Create the wavelength grid
    # wgrid = setup_wgrid(start=350 * u.nm, stop=1000 * u.nm, bin=2. * u.nm)

    WMIN = 4700E-10 * u.m
    WMAX = 24500E-10 * u.m
    NWL_GRID = 3096
    wgrid = np.linspace(WMIN, WMAX, NWL_GRID)

    mwext = swig_mw_extinction.mw_extinction(wgrid.value, 'odonnell')

    # Set extinction maps (Schlegel)
    mwext.set_Schlegel_maps(sn_filepath, ss_filepath)

    return mwext


def calculate_av_old(mwext, catalog):
    """
    Measure the A_v parameter for each star in the catalogue
    """

    return np.array([mwext.Av_from_Schlegel(record['RA'], record['DEC']) for record in catalog[1].data])

def calculate_av(mwext, catalog):
    """
    Measure the A_v parameter for each star in the catalogue
    """

    return np.array([mwext.Av_from_Schlegel(record['ra'], record['dec']) for record in catalog])

def get_av(mwext, ra, dec):
    return mwext.Av_from_Schlegel(ra, dec)

def verify_av(mwext, gt_filepath):
    # Read catalogue which will be used to validate the A_v calculation
    gt_catalog = read_catalog(gt_filepath)

    # Print the results
    print(f'RA\t\tDec\tAv_1\tAv_2\tDiff')
    for i, record in enumerate(gt_catalog[1].data):
        ra, dec, av_org = record['RA'], record['DEC'], record['AV']
        av = mwext.Av_from_Schlegel(record['RA'], record['DEC'])
        print(f'{ra:2.2f}\t{dec:2.2f}\t{av_org:2.3f}\t{av:2.3f}\t{av_org-av:2.3f}')


if __name__ == '__main__':
    ROOT = "/home/denis/Development/ELViS/work_dir/"
    SCHLEGEL_NORTH_FILEPATH = 'SFD_dust_4096_ngp.fits'  # https://github.com/kbarbary/sfddata
    SCHLEGEL_SOUTH_FILEPATH = 'SFD_dust_4096_sgp.fits'  # https://github.com/kbarbary/sfddata
    CATALOG = ROOT + "ppos/gaia_star_catalog.fits"
    # SCHLEGEL_NORTH_FILEPATH = 'HFI_CompMap_ThermalDustModel_2048_R1.20_V2' # http://dss-mdb.euclid.astro.rug.nl/HFI_CompMap_ThermalDustModel_2048_R1.20_V2.fits
    GROUND_TRUTH_CATALOG = ROOT + "ppos/PLAN-SIM_SC8_GSIR_SWF3_PPO_1_091120-PPO-0-201109-115132-639/data/EUC_SIM_TUSTARCAT-37729_0BC97E1E92FA-0041495_20210216T132528.457005Z_v13_SC8_MAIN_small.fits"
    VALIDATE = False
    


    # Read catalogue to which we will add the A_v
    catalog = read_catalog(CATALOG)

    mwext = get_mwext(SCHLEGEL_NORTH_FILEPATH, SCHLEGEL_SOUTH_FILEPATH)

    # Write AV column to the catalog
    catalog[1].data['AV'] = calculate_av(mwext, catalog)

    # Get Sed
    spectrum_vector = None

    # Apply reddening to the SEDs
    # mwext.apply_mw_extinction(spectrum_vector, catalog[1].data['AV'])

    catalog.close()

    if VALIDATE:
        # Validate the AV calculation by comparing it with the existing ELViS star catalog
        verify_av(mwext, GROUND_TRUTH_CATALOG)
