# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.1
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _swig_emission
else:
    import _swig_emission

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)


class SwigPyIterator(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _swig_emission.delete_SwigPyIterator

    def value(self):
        return _swig_emission.SwigPyIterator_value(self)

    def incr(self, n=1):
        return _swig_emission.SwigPyIterator_incr(self, n)

    def decr(self, n=1):
        return _swig_emission.SwigPyIterator_decr(self, n)

    def distance(self, x):
        return _swig_emission.SwigPyIterator_distance(self, x)

    def equal(self, x):
        return _swig_emission.SwigPyIterator_equal(self, x)

    def copy(self):
        return _swig_emission.SwigPyIterator_copy(self)

    def next(self):
        return _swig_emission.SwigPyIterator_next(self)

    def __next__(self):
        return _swig_emission.SwigPyIterator___next__(self)

    def previous(self):
        return _swig_emission.SwigPyIterator_previous(self)

    def advance(self, n):
        return _swig_emission.SwigPyIterator_advance(self, n)

    def __eq__(self, x):
        return _swig_emission.SwigPyIterator___eq__(self, x)

    def __ne__(self, x):
        return _swig_emission.SwigPyIterator___ne__(self, x)

    def __iadd__(self, n):
        return _swig_emission.SwigPyIterator___iadd__(self, n)

    def __isub__(self, n):
        return _swig_emission.SwigPyIterator___isub__(self, n)

    def __add__(self, n):
        return _swig_emission.SwigPyIterator___add__(self, n)

    def __sub__(self, *args):
        return _swig_emission.SwigPyIterator___sub__(self, *args)
    def __iter__(self):
        return self

# Register SwigPyIterator in _swig_emission:
_swig_emission.SwigPyIterator_swigregister(SwigPyIterator)

class IntVector(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def iterator(self):
        return _swig_emission.IntVector_iterator(self)
    def __iter__(self):
        return self.iterator()

    def __nonzero__(self):
        return _swig_emission.IntVector___nonzero__(self)

    def __bool__(self):
        return _swig_emission.IntVector___bool__(self)

    def __len__(self):
        return _swig_emission.IntVector___len__(self)

    def __getslice__(self, i, j):
        return _swig_emission.IntVector___getslice__(self, i, j)

    def __setslice__(self, *args):
        return _swig_emission.IntVector___setslice__(self, *args)

    def __delslice__(self, i, j):
        return _swig_emission.IntVector___delslice__(self, i, j)

    def __delitem__(self, *args):
        return _swig_emission.IntVector___delitem__(self, *args)

    def __getitem__(self, *args):
        return _swig_emission.IntVector___getitem__(self, *args)

    def __setitem__(self, *args):
        return _swig_emission.IntVector___setitem__(self, *args)

    def pop(self):
        return _swig_emission.IntVector_pop(self)

    def append(self, x):
        return _swig_emission.IntVector_append(self, x)

    def empty(self):
        return _swig_emission.IntVector_empty(self)

    def size(self):
        return _swig_emission.IntVector_size(self)

    def swap(self, v):
        return _swig_emission.IntVector_swap(self, v)

    def begin(self):
        return _swig_emission.IntVector_begin(self)

    def end(self):
        return _swig_emission.IntVector_end(self)

    def rbegin(self):
        return _swig_emission.IntVector_rbegin(self)

    def rend(self):
        return _swig_emission.IntVector_rend(self)

    def clear(self):
        return _swig_emission.IntVector_clear(self)

    def get_allocator(self):
        return _swig_emission.IntVector_get_allocator(self)

    def pop_back(self):
        return _swig_emission.IntVector_pop_back(self)

    def erase(self, *args):
        return _swig_emission.IntVector_erase(self, *args)

    def __init__(self, *args):
        _swig_emission.IntVector_swiginit(self, _swig_emission.new_IntVector(*args))

    def push_back(self, x):
        return _swig_emission.IntVector_push_back(self, x)

    def front(self):
        return _swig_emission.IntVector_front(self)

    def back(self):
        return _swig_emission.IntVector_back(self)

    def assign(self, n, x):
        return _swig_emission.IntVector_assign(self, n, x)

    def resize(self, *args):
        return _swig_emission.IntVector_resize(self, *args)

    def insert(self, *args):
        return _swig_emission.IntVector_insert(self, *args)

    def reserve(self, n):
        return _swig_emission.IntVector_reserve(self, n)

    def capacity(self):
        return _swig_emission.IntVector_capacity(self)
    __swig_destroy__ = _swig_emission.delete_IntVector

# Register IntVector in _swig_emission:
_swig_emission.IntVector_swigregister(IntVector)

class DoubleVector(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def iterator(self):
        return _swig_emission.DoubleVector_iterator(self)
    def __iter__(self):
        return self.iterator()

    def __nonzero__(self):
        return _swig_emission.DoubleVector___nonzero__(self)

    def __bool__(self):
        return _swig_emission.DoubleVector___bool__(self)

    def __len__(self):
        return _swig_emission.DoubleVector___len__(self)

    def __getslice__(self, i, j):
        return _swig_emission.DoubleVector___getslice__(self, i, j)

    def __setslice__(self, *args):
        return _swig_emission.DoubleVector___setslice__(self, *args)

    def __delslice__(self, i, j):
        return _swig_emission.DoubleVector___delslice__(self, i, j)

    def __delitem__(self, *args):
        return _swig_emission.DoubleVector___delitem__(self, *args)

    def __getitem__(self, *args):
        return _swig_emission.DoubleVector___getitem__(self, *args)

    def __setitem__(self, *args):
        return _swig_emission.DoubleVector___setitem__(self, *args)

    def pop(self):
        return _swig_emission.DoubleVector_pop(self)

    def append(self, x):
        return _swig_emission.DoubleVector_append(self, x)

    def empty(self):
        return _swig_emission.DoubleVector_empty(self)

    def size(self):
        return _swig_emission.DoubleVector_size(self)

    def swap(self, v):
        return _swig_emission.DoubleVector_swap(self, v)

    def begin(self):
        return _swig_emission.DoubleVector_begin(self)

    def end(self):
        return _swig_emission.DoubleVector_end(self)

    def rbegin(self):
        return _swig_emission.DoubleVector_rbegin(self)

    def rend(self):
        return _swig_emission.DoubleVector_rend(self)

    def clear(self):
        return _swig_emission.DoubleVector_clear(self)

    def get_allocator(self):
        return _swig_emission.DoubleVector_get_allocator(self)

    def pop_back(self):
        return _swig_emission.DoubleVector_pop_back(self)

    def erase(self, *args):
        return _swig_emission.DoubleVector_erase(self, *args)

    def __init__(self, *args):
        _swig_emission.DoubleVector_swiginit(self, _swig_emission.new_DoubleVector(*args))

    def push_back(self, x):
        return _swig_emission.DoubleVector_push_back(self, x)

    def front(self):
        return _swig_emission.DoubleVector_front(self)

    def back(self):
        return _swig_emission.DoubleVector_back(self)

    def assign(self, n, x):
        return _swig_emission.DoubleVector_assign(self, n, x)

    def resize(self, *args):
        return _swig_emission.DoubleVector_resize(self, *args)

    def insert(self, *args):
        return _swig_emission.DoubleVector_insert(self, *args)

    def reserve(self, n):
        return _swig_emission.DoubleVector_reserve(self, n)

    def capacity(self):
        return _swig_emission.DoubleVector_capacity(self)
    __swig_destroy__ = _swig_emission.delete_DoubleVector

# Register DoubleVector in _swig_emission:
_swig_emission.DoubleVector_swigregister(DoubleVector)

class emission(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _swig_emission.emission_swiginit(self, _swig_emission.new_emission())
    __swig_destroy__ = _swig_emission.delete_emission

    def apply_emission_lines(self, wave_in, spectrum, abs_mag_evo_r01, redshift, logf_halpha_ext, logf_hbeta_ext, logf_o2_ext, logf_o3_ext, logf_n2_ext, logf_s2_ext, vacuum=True):
        return _swig_emission.emission_apply_emission_lines(self, wave_in, spectrum, abs_mag_evo_r01, redshift, logf_halpha_ext, logf_hbeta_ext, logf_o2_ext, logf_o3_ext, logf_n2_ext, logf_s2_ext, vacuum)

    def apply_integrated_emission_lines(self, wave_limits, spectrum, abs_mag_evo_r01, redshift, logf_halpha_ext, logf_hbeta_ext, logf_o2_ext, logf_o3_ext, logf_n2_ext, logf_s2_ext, vacuum=True):
        return _swig_emission.emission_apply_integrated_emission_lines(self, wave_limits, spectrum, abs_mag_evo_r01, redshift, logf_halpha_ext, logf_hbeta_ext, logf_o2_ext, logf_o3_ext, logf_n2_ext, logf_s2_ext, vacuum)

# Register emission in _swig_emission:
_swig_emission.emission_swigregister(emission)



