import matplotlib.pyplot as plt


def figure(n_figures):
    fig, axes = plt.subplots(1, n_figures, figsize=(4 * n_figures, 6))

    return fig, axes


def plot_image(ax, image, labels, v_limits=(None, None), extent=None):
    title, x_label, y_label = labels
    vmin, vmax = v_limits
    ax.imshow(image, origin='lower', aspect='equal', vmin=vmin, vmax=vmax, extent=extent)
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    ax.set_xlim((0, image.shape[1]))
    ax.set_ylim((0, image.shape[0]))

    return ax


def plot_h_image(ax, image, labels, v_limits=(None, None), extent=None):
    title, x_label, y_label = labels
    vmin, vmax = v_limits
    ax.imshow(image, origin='lower', aspect='equal', vmin=vmin, vmax=vmax, extent=extent)
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    ax.set_aspect(0.002)
    # ax.set_xlim((0, image.shape[1]))
    # ax.set_ylim((0, image.shape[0]))

    return ax

def plot_hough_image(image, labels, d, theta, v_limits=(None, None)):
    fig, axes = figure(3)

    title, x_label, y_label = labels
    vmin, vmax = v_limits
    n = 5
    for i, ax in enumerate(axes):
        extent = [theta[0], theta[-1], d[0], d[-1]]
        ax.imshow(image[i * n * 2: i * n * 2 + n * 2], origin='lower', vmin=vmin, vmax=vmax, extent=extent)



def plot_line_candidates(ax, image, labels, line_candidates, v_limits=(None, None)):
    ax = plot_image(ax, image, labels, v_limits=v_limits)
    for candidate in line_candidates:
        x, y = candidate
        ax.plot(x, y, '-r')

    ax.set_xlim((0, image.shape[1]))
    ax.set_ylim((0, image.shape[0]))

    return ax


def display_plots(block=True):
    plt.tight_layout()
    plt.show(block=block)
