"""
Aims to assign a template SED to Gaia catalogue stars, by matching their Gaia photometer colours.

There are several steps in this process:
(1) read the FITS file of BaSeL library star SEDs (Lejeune et al 1998 et seq)
(2) read the Gaia, VIS and H passband definitions
(3) Use (2) to calculate the expected Gaia photometer colours for the SEDs from (1), also their VIS-Gaia colour
(4) Read a Gaia catalogue containg photometer information
(5) Match the Gaia catalogue colours from (4) with the SED colours from (3)
(6) Output the matching index which is the assignment of temnplate SED to each Gaia star
(7) Also outout some sanity check plots of SED-temperature v. Gaia-temperature and VISmagnitude v. Gaia G-magnitude

Note that faint stars do not have Gaia temperature values and the faintest stars do not have Gaia photometer values

The code requires the synphot photometry library https://synphot.readthedocs.io/en/latest/
and associated calibration files https://synphot.readthedocs.io/en/latest/#installation-and-setup which should be downloaded automatically

Because we want to calculate the VIS magnitudes, we also need to run the PSF Toolkit to extract the VIS passband.
It will expect environment variable PYTHONPATH to be set to the PSF Toolkit build location.

Lance.Miller@physics.ox.ac.uk
August 2021
updateed 13/8/21 to include H band calculation

"""

# standard imports
import matplotlib.pyplot as plt
import os
import numpy as np
import astropy.io.fits as fits
import glob
import sys

# imports for handling VIS throughput from PSF Toolkit
try:
    from utils import VIS_response
    vis_available = True
except Exception as e:
    print (f'VIS response not available. Reason {e}')
    vis_available = False

# imports for handling HST/ACS photometry from synphot
# these can be imported from AstroConda
# see https://stsynphot.readthedocs.io/en/latest/
# see https://synphot.readthedocs.io/en/latest/index.html
from synphot import SourceSpectrum, Observation, SpectralElement, units
from synphot.models import ConstFlux1D, Empirical1D
from astropy import units as u

# define Euclid effective area in cm**2
vis_telescope_diameter = 120. # cm
vis_m2_diameter = 39.5 # diameter of M2 obscuration, cm
visarea = np.pi*(np.square(vis_telescope_diameter/2.)-np.square(vis_m2_diameter/2.)) * units.AREA

# define Gaia effective area
# actually as here we only measure colours, the results don't depend on area so just set a nominal value
gaiaarea = visarea


def read_H_passband():

    # path to the FITS file containing the H passband
    # note this must the same as used inside ELViS
    pfile = 'EUC_SIM_FILTER-H-2MASS_20190312T161600.000000Z_0.0.fits'

    if os.path.exists(pfile):
        print (' reading H passband file ',pfile)
    else:
        raise FileNotFoundError(f' H passband file {pfile} not found')

    hdu = fits.open(pfile)

    # read table of throughput values, assuming
    # first column contains wavelength in Agstrom units
    # second column contains throughput (dimensionless)
    # third column is ignored
    data = hdu[1].data
    wlenh = data.field(0)
    thband = data.field(1)

    hdu.close()

    return wlenh, thband


def read_Gaia_passbands():
    """
    read the file defining the Gaia passbands and select the ones we want
    """

    # path to passbands ascii file
    # updated to latest DR2 passband definitions, it doesn't make much difference
    # adjust as necessary!
    # pfile = '../Gaia/GaiaDR2_Passbands.dat'
    pfile = 'GaiaDR2_RevisedPassbands.dat'

    if os.path.exists(pfile):
        print (' reading Gaia passbands file ',pfile)
    else:
        raise FileNotFoundError(f' Gaia passbands file {pfile} not found')

    gaia_bands = np.loadtxt(pfile)

    return gaia_bands



def readSED(sedfile):
    """
    read the FITS file of SED templates supplied with ELViS
    This is based on the SEDs of Lejeune et al (1998), BaSeL library
    """

    if os.path.exists(sedfile):
        print (' reading SED file ',sedfile)
    else:
        raise FileNotFoundError(f' SED  file {sedfile} not found')

    hdu = fits.open(sedfile)

    head = hdu[1].header
    if head["EXTNAME"] != "LAMBDA":
        hdu.info()
        raise ValueError(' unexpected FITS extension name, not LAMBDA')

    # convert wavelength units to Angstrom
    wlens = hdu[1].data * 10

    head = hdu[2].header
    if head["EXTNAME"] != "SED":
        hdu.info()
        raise ValueError(' unexpected FITS extension name, not SED')

    seds = hdu[2].data

    head = hdu[3].header
    if head["EXTNAME"] != "SED_PARAM":
        hdu.info()
        raise ValueError(' unexpected FITS extension name, not SED_PARAM')

    params = hdu[3].data

    hdu.close()

    return wlens, seds, params



def get_bandpasses():
    """
    convert the VIS, Gaia passbands into synphot format
    and calculate the magnitude zero points.

    note that Gaia magnitudes use the Vega magnitude system, not AB.
    note also that synphot works in wavelength units of Angstroms by default
    """

    # Gaia magnitudes are in the Vega system, so set AB_magnitudes to be False
    AB_magnitudes = False

    # set an arbitrary reference magnitude - we are only measuring colours
    refmag = 0.

    # define the calibration spectrum
    if AB_magnitudes:
        # define a calibration flux in AB magnitudes at the supplied reference magnitude
        sp = SourceSpectrum(ConstFlux1D, amplitude=refmag*u.ABmag)
    else:
        sp = SourceSpectrum.from_vega()

    # read the tables of the Gaia passbands
    gaia_bands = read_Gaia_passbands()

    # select the Gaia bands of interest: Bp, Rp, G
    gaia_passband_index = (3,5,1)

    # start the list containing the bandpass definitions
    # this is four elements for VIS followed by the three Gaia bands
    bandlist = []
    # start the corresponding list of four zeropoint magnitudes
    zplist = []

    # calculate the VIS zeropoint
    if vis_available:
        # get VIS throughput and wavelengths as synphot
        bpass, wls = VIS_response()
        # wls /= 10.
        bandlist.append((bpass,wls))
        zp = Observation(sp,bpass,binset=wls)
        zplist.append(zp.countrate(visarea).value)

    else:
        raise ValueError('VIS requested but not available')

    # calculate the zeropoints for the three Gaia bands
    for i in gaia_passband_index:
        good = gaia_bands[:,i]<99
        wls = 10 * gaia_bands[good,0]  # convert wavelength units from nm to Angstrom
        bpass = SpectralElement(Empirical1D, points=wls, lookup_table=gaia_bands[good,i])
        bandlist.append((bpass,wls))
        zp = Observation(sp,bpass,binset=wls)
        zplist.append(zp.countrate(gaiaarea).value)

    # add the H band onto the end of the list
    # read the H passband
    # wavelength is already in Angstrom (assumed)
    wls, thband = read_H_passband()

    bpass = SpectralElement(Empirical1D, points=wls, lookup_table=thband)
    bandlist.append((bpass,wls))
    zp = Observation(sp,bpass,binset=wls)
    zplist.append(zp.countrate(visarea).value)

    return bandlist, zplist, refmag


def get_mags(seds, wlens, bandlist, zplist, refmag):
    """
    work the through the supplied SED templates and calculate their expected
    Gaia and VIS magnitudes
    """

    # initialise array containing the magnitude values
    mag = np.zeros([len(seds),len(bandlist)])

    # go through the SEDs and generate their colours in each of the three Gaia bands and VIS
    for k,s in enumerate(seds):

        # convert each SED to a synphot spectrum
        spectrum = SourceSpectrum(Empirical1D, points=wlens, lookup_table=s*units.FNU)

        for i, bp in enumerate(bandlist):

            # check that bandpasses and spectrum overlap at this redshift
            if bp[0].check_overlap(spectrum) == 'full':
                # calculate magnitudes
                bandobs = Observation(spectrum,bp[0],binset=bp[1])
                counts = bandobs.countrate(visarea)
                mag[k,i] = refmag - 2.5*np.log10(counts.value/zplist[i])
                #vspec = bandobs(bp[1], flux_unit='count', area=visarea)
                #plt.plot(bp[1], vspec, label=band_name[i])
            else:
                raise ValueError(' failed band overlap check')

    return mag


def read_gaia_fits(gaia_fits):
    """
    read the gaia fits table file
    input: gaia_fits: filename
    return: data: fits table
    """

    if os.path.exists(gaia_fits):
        print (' reading Gaia catalogue file ',gaia_fits)
    else:
        raise FileNotFoundError(f' Gaia file {gaia_fits} not found')

    hdu = fits.open(gaia_fits)
    data = hdu[1].data
    return data


def colour_match(omags, smags):
    """
    match the SED colours to the Gaia colours and return the match index
    """

    # subtract mean magnitudes
    omean = np.mean(omags, axis=1)
    smean = np.mean(smags, axis=1)
    omags = (omags.T-omean).T
    smags = (smags.T-smean).T

    # least-squares match
    idx = np.zeros(len(omags), dtype=int)
    minvar = np.zeros(len(omags))
    for i,m in enumerate(omags):
        var = np.sum(np.square((m-smags)),axis=-1)
        varidx = np.argmin(var)
        idx[i] = varidx
        minvar[i] = var[varidx]

    return idx, minvar

def colour_match2(omags, smags):
    """
    match the SED olours to the Gaia colours and return the match index
    """

    # subtract mean magnitudes
    omean = np.mean(omags, axis=0)
    smean = np.mean(smags, axis=1)
    omags = (omags.T-omean).T
    smags = (smags.T-smean).T

    # least-squares match
    idx = np.zeros(len(omags), dtype=int)
    minvar = np.zeros(len(omags))
    for i,m in enumerate(omags):
        var = np.sum(np.square((m-smags)),axis=-1)
        varidx = np.argmin(var)
        idx[i] = varidx
        minvar[i] = var[varidx]

    return idx, minvar

if __name__ == '__main__':
    """
    main code entry point
    """

    # read the catalogue of SEDs
    # adjust path as necessary!
    wlens, seds, params = readSED('EUC-TEST-SEDLIB-2013-11-14_MODIF.fits')
    nspec, nlambda = seds.shape

    # get the spectrum parameters that were read from the SEDs file
    logg = params.field(0)
    teff = params.field(1)
    feh  = params.field(2)

    # select dwarf stars (high gravity), with reasonable feh metallicity, and not too hot from the SED table
    starsel = (logg>3.5) & (feh>-1) & (teff<12000)
    # remember the selected indices in the original array
    sedindex = np.arange(nspec, dtype=int)
    sedindex = sedindex[starsel]

    # create the VIS, Gaia bandpasses and zeropoint definitions
    bandlist, zplist, refmag = get_bandpasses()

    # get the magnitudes in each band of the selected template SEDs
    sed_mags = get_mags(seds[starsel], wlens, bandlist, zplist, refmag)

    # get Gaia data from a pre-prepared file at the NEP
    # for the polar cap at elat>80 degrees
    gaia_fits_table = 'ecl_80_91-result.fits.gz'
    gdata = read_gaia_fits(gaia_fits_table)
    elon = gdata['ecl_lon']
    elat = gdata['ecl_lat']
    rpmag = gdata['phot_rp_mean_mag']
    bpmag = gdata['phot_bp_mean_mag']
    gmag = gdata['phot_g_mean_mag']
    gteff = gdata['teff_val']

    # select Gaia stars near the NEP
    # (change this to match the stars selected in an actual VIS field)
    sel = elat >= 89.5

    # only keep catalogue values for these NEP-selected stars
    elon = elon[sel]
    elat = elat[sel]
    rpmag = rpmag[sel]
    bpmag = bpmag[sel]
    gmag = gmag[sel]
    gteff = gteff[sel]

    # package selected magnitudes into array
    observed_mags = np.zeros([len(rpmag), 3])
    observed_mags[:,0] = bpmag
    observed_mags[:,1] = rpmag
    observed_mags[:,2] = gmag

    # identify gaia stars with unmeasured colours
    no_colours = np.isnan(rpmag) | np.isnan(bpmag)

    # for those stars without measured colours, find the median colour at faint magnitudes and use that instead
    sel = (gmag > 18.) & ~no_colours
    gbp = np.median(gmag[sel]-bpmag[sel])
    grp = np.median(gmag[sel]-rpmag[sel])
    observed_mags[no_colours,0] = observed_mags[no_colours,2] - gbp
    observed_mags[no_colours,1] = observed_mags[no_colours,2] - grp

    # filter for valid temperature values in the gaia data (only used for the final comparison test)
    # note only bright stars have Gaia temperatures
    gaia_temp_exists = ~np.isnan(gteff)
    gteff[np.isnan(gteff)] = -99

    # match Gaia star colours to SED colours
    # (strip the VIS,H magnitudes off the SED mags for the colour matching, as VIS,H not measured by gaia)
    gaia_match_index, minvar = colour_match(observed_mags, sed_mags[:,1:4])


    # match this index back to the index in the original SED table
    # (remember that we excluded some SEDs by the selection criteria)
    sedmatch = sedindex[gaia_match_index]

    # find the SED temperature for the matches
    matched_teff = teff[sedmatch]

    # plot SED temperature versus Gaia temperature as a sanity check
    # for those (bright) Gaia stars which have a valid gaia temperature
    plt.figure()
    plt.plot(gteff[gaia_temp_exists], matched_teff[gaia_temp_exists],'.')
    xmin = np.min(gteff[gaia_temp_exists])
    xmax = np.max(gteff[gaia_temp_exists])
    plt.plot((xmin,xmax),(xmin,xmax),color='k')
    plt.xlabel('Gaia T_eff')
    plt.ylabel('SED T_eff')
    plt.savefig('temperatures.png')
    plt.close()

    # find the VIS magnitude from the best matching SED
    # this uses the selected sed_mags array
    # and calculates  vismag = sed_mag(VIS band) + ( Gaia G mag ) - sed_mag(G band)
    vismag = sed_mags[gaia_match_index,0] + gmag - sed_mags[gaia_match_index,3]

    # likewise for the H band
    Hmag = sed_mags[gaia_match_index,4] + gmag - sed_mags[gaia_match_index,3]

    # sanity check plot of predicted VIS and H mag against gaia G mag
    # there should be some scatter, but overall a decent correlation
    plt.figure()
    plt.plot(gmag,vismag,'.',ms=1,label='VIS')
    plt.plot(gmag,Hmag,'.',ms=1,label='H')
    plt.xlabel('Gaia G mag')
    plt.ylabel('SED mag')
    plt.legend()
    plt.savefig('magnitudes.png')
    plt.close()

    # sanity check plot of predicted SED temp v. VIS mag
    plt.figure()
    plt.plot(vismag,matched_teff,'.',ms=1.)
    plt.xlabel('SED VIS mag')
    plt.ylabel('SED T_eff')
    plt.savefig('mag-temp.png')
    plt.close()

    # sanity check plot of match rms v. VIS mag
    plt.figure()
    plt.plot(vismag,np.sqrt(minvar),'.',ms=1.)
    plt.xlabel('SED VIS mag')
    plt.ylabel('match rms')
    plt.savefig('mag-rms.png')
    plt.close()

    """
    # sanity check plot of predicted logg v feh
    plt.figure()
    plt.plot(logg[sedmatch], feh[sedmatch],'.',ms=1.)
    plt.xlabel('log(g)')
    plt.ylabel('Fe/H')    
    plt.savefig('logg-feh.png')
    plt.close()
    """

    # output the Gaia values, VIS magnitude, gaia and fitted temperatures, matching index
    cols = np.column_stack((elon,elat,rpmag,bpmag,gmag,vismag,Hmag,gteff,matched_teff,sedmatch))
    hstring=' elon       elat      Rp     Bp     G     VIS     H     T_Gaia   T_SED SED_index'
    np.savetxt('gaia_NEP_seds.txt',cols,fmt='%10.5f %10.5f %6.3f %6.3f %6.3f %6.3f %6.3f %7.0f %7.0f %d',header=hstring)
