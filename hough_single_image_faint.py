import os

import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
import numba
import numpy as np
from skimage.filters import threshold_triangle
from sklearn.cluster import DBSCAN

# Hough transform method to search for three diffraction spikes in an image
# containing a single bright star
# LM Dec 2020

# default plotting setup
params = {'legend.fontsize': 'x-large',
          'axes.labelsize': 'x-large',
          'axes.titlesize': 'x-large',
          'xtick.labelsize': 'x-large',
          'ytick.labelsize': 'x-large'}
plt.rcParams.update(params)


@numba.jit('float64[:,:](float64[:,:], int64[:], int64[:], int64, int64[:,:])', nopython=True)
def make_hough_transform(im, xp, yp, maxr, reg):
    """
    make hough transform

    this implementation is a bit unconventional.  It defines the set of pixels around the
    rectangular boundary of the image, and considers all possible lines that join
    any 2 points on the boundary, working out the Hough transform for each line.
    To speed things up and reduce contamination, the search is performed only in a region of
    line parameters that are close to where we expect the spikes to be.

    """

    xlen = len(xp)
    hval = np.zeros((xlen, xlen))
    ydim, xdim = np.shape(im)

    # define the search range about each of the three nominal lines, expressed as
    # the plus/minus range of boundary pixel coordinates
    # dr = 20
    dr = 100

    # go through pairs of perimeter coordinates and find contribution from image
    lenyp = len(yp)
    # the reg array defines three lines in the image space.
    # only the parameter regions close to the lines defined in the reg array are searched
    for k in range(len(reg)):
        for i in range(reg[k, 0] - dr, reg[k, 0] + dr + 1):
            for j in range(reg[k, 1] - dr, reg[k, 1] + dr + 1):
                xmin = min(xp[i], xp[j])
                xmax = max(xp[i], xp[j])
                ymin = min(yp[i], yp[j])
                ymax = max(yp[i], yp[j])

                xmax = min(xmax, xdim // 2 + maxr)
                xmin = max(xmin, xdim // 2 - maxr)
                ymax = min(ymax, ydim // 2 + maxr)
                ymin = max(ymin, ydim // 2 - maxr)

                # cope with lines that may be purely horizontal or purely vertical
                if ymax - ymin > xmax - xmin:
                    # more extended in y direction
                    for iy in range(ymin, ymax + 1):
                        ix = int(xp[i] + (iy - yp[i]) * (xp[j] - xp[i]) / (yp[j] - yp[i]) + 0.5)
                        hval[i, j] += im[iy, ix]
                elif xmax > xmin:
                    # more extended in x direction
                    for ix in range(xmin, xmax + 1):
                        iy = int(yp[i] + (ix - xp[i]) * (yp[j] - yp[i]) / (xp[j] - xp[i]) + 0.5)
                        hval[i, j] += im[iy, ix]

    shval = np.copy(hval)

    # and now smooth in width by +/- pixel
    for i in range(lenyp - 1):
        im = (i + lenyp - 1) % lenyp
        ip = (i + 1) % lenyp
        for j in range(i + 1, len(yp)):
            jm = (j + lenyp - 1) % lenyp
            jp = (j + 1) % lenyp
            shval[i, j] += 0.5 * (hval[im, jm] + hval[ip, jp])

    return shval


def load_fits(path, ext):
    """
    Loads a FITS File.
    :param path: Path to where FITS file is stored.
    :return: FITS data, in np.array format.
    """
    hdulist = pyfits.open(path)
    data = hdulist[ext].data
    print(path, ' extension ', ext, np.shape(data))

    hdulist.close()
    return data


def save_fits(data, path):
    """
    Save FITS data.
    :param data: FITS data to save, np.array format.
    :param path: Path to where data is to be saved.
    """
    hdu = pyfits.PrimaryHDU(data)
    hdulist = pyfits.HDUList([hdu])
    hdulist.writeto(path, overwrite=True)
    return


def naive_dbscan(test_image):
    # mean, med, std = sigma_clipped_stats(test_image)
    # test_image -= mean

    def get_clusters(ndx, c_labels):
        # Add cluster labels to the data
        labelled_data = np.append(ndx, np.expand_dims(c_labels, axis=1), axis=1)

        # Cluster mask to remove noise clusters
        de_noised_data = labelled_data[labelled_data[:, 3] > -1]

        de_noised_data = de_noised_data[de_noised_data[:, 3].argsort()]

        # Return the location at which clusters where identified
        cluster_ids = np.unique(de_noised_data[:, 3], return_index=True)

        # Split the data into clusters
        clusters = np.split(de_noised_data, cluster_ids[1])

        # remove empty clusters
        clusters = [x for x in clusters if np.any(x)]

        return clusters

    local_thresh = threshold_triangle(test_image, nbins=1024)
    local_filter_mask = test_image < local_thresh

    test_image[local_filter_mask] = 0

    noise_est = 0

    db_scan = DBSCAN(eps=30, min_samples=5, algorithm='kd_tree', n_jobs=-1)

    ndx = np.column_stack(np.where(test_image > noise_est))

    ndx = np.append(ndx, np.expand_dims(test_image[test_image > noise_est], axis=1), axis=1)

    try:
        # Perform (2D) clustering on the data and returns cluster labels the points are associated with
        c_labels = db_scan.fit_predict(ndx)
    except ValueError:
        return []

    if len(c_labels) < 1:
        return []

    return get_clusters(ndx, c_labels)


def hough_transform(image, df, radius=20, maxr=100):
    # define maximum radius away from star where spikes will be measured
    radsq = radius * radius
    maxrsq = maxr * maxr

    # image[image > 3000] = 0

    # define image coordinates
    y = np.arange(np.shape(image)[0], dtype=np.int64)
    x = np.arange(np.shape(image)[1], dtype=np.int64)

    # find flux centroid in order to centre the star - this is very simple, only
    # works if the flux is dominated by a single star
    xcen = np.sum(x * np.mean(image, axis=0)) / np.sum(np.mean(image, axis=0))
    ycen = np.sum(y * np.mean(image, axis=1)) / np.sum(np.mean(image, axis=1))

    # xcen = 200
    # ycen = 240

    print('centre', xcen, ycen)


    # define a circular-symmetric pixel weight function to optimise the detection
    # selecting pixels between min, max radii
    xv, yv = np.meshgrid(x - xcen, y - ycen)
    rsq = xv * xv + yv * yv
    # weight = np.square(np.sinc(np.sqrt(rsq)/maxr/2.))
    weight = np.ones_like(image)
    weight[rsq < radsq] = 0.
    # weight[rsq > maxrsq] = 0.

    # also exclude pixels up the saturated/bleeding column for that very bright star
    # weight[(xv<=10)&(xv>=-10)] = 0.

    image *= weight

    plt.imshow(image, origin='lower', aspect='equal', vmin=1999, vmax=2055)
    np.save("masked_star2.npy", image)
    plt.colorbar()
    # make arrays of perimeter coordinates, working around the image boundary and
    # making arrays of the pixel x,y values in the boundary
    xlen = len(x)
    xp = np.zeros(2 * (2 * xlen - 2), dtype=np.int64)
    xp[:xlen] = x
    xp[xlen:2 * xlen - 2] = x[-1] + np.zeros(xlen - 2, dtype=np.int64)
    xp[2 * xlen - 2:3 * xlen - 2] = x[-1] - x
    xp[3 * xlen - 2:] = np.zeros(xlen - 2, dtype=np.int64)

    yp = np.zeros(2 * (2 * xlen - 2), dtype=np.int64)
    yp[:xlen] = np.zeros(xlen, dtype=np.int64)
    yp[xlen:2 * xlen - 2] = y[1:-1]
    yp[2 * xlen - 2:3 * xlen - 2] = y[-1] + np.zeros(xlen, dtype=np.int64)
    yp[3 * xlen - 2:] = y[-1] - y[1:-1]

    # plt.imshow(image, origin='lower', aspect='equal', vmin=np.min(image), vmax=np.max(image) / 2.)
    # plt.show()

    # define the regions in parameter space to be searched.
    # There are three search regions corresponding to the spikes at three angles
    # each region is defined as a line between two boundary pixels.  the array
    # values are the indices of those boundary pixels.
    reg = np.array([[767, 1720], [436, 1401], [173, 1124]])
    # reg = np.array([[436, 1401], [173, 1124]])

    # measure the hough transform for all relevant pairs of lines
    val = make_hough_transform(image, xp, yp, maxr, reg)

    val /= np.max(val)

    # calculate line slopes and make plots

    plt.figure()
    plt.imshow(val)
    plt.savefig('hough_transform.png')
    plt.close()

    dr = 10
    xi = np.zeros(len(reg), dtype=int)
    yi = np.zeros(len(reg), dtype=int)
    for k, r in enumerate(reg):
        sim = val[r[0] - dr:r[0] + dr, r[1] - dr:r[1] + dr]
        p = np.argmax(sim.flatten())
        yi[k] = p // (2 * dr)
        xi[k] = p - yi[k] * 2 * dr
        # print ('max at ',xi[k]+r[1]-dr,yi[k]+r[0]-dr,sim[yi[k],xi[k]])
        xi[k] += r[1] - dr
        yi[k] += r[0] - dr

    isect = []
    slope = []

    # from skimage.transform import hough_line, hough_line_peaks
    # from matplotlib import cm
    # from skimage.feature import canny
    # from skimage.draw import line
    # from skimage.feature import canny
    # from skimage.draw import line
    # from skimage.transform import probabilistic_hough_line
    #
    # tested_angles = np.linspace(-np.pi / 4, np.pi / 4, 180, endpoint=False)
    # # tested_angles = np.arange(-1.3, -1.2, 0.01)
    # h, theta, d = hough_line(image, theta=tested_angles)
    #
    # edges = canny(image, 2, 1, 25)
    # lines = probabilistic_hough_line(edges, threshold=10, line_length=5,
    #                                  line_gap=3)
    #
    # # Generating figure 1
    # fig, axes = plt.subplots(1, 4, figsize=(15, 6))
    # ax = axes.ravel()
    #
    # ax[0].imshow(val)
    # ax[0].set_title('Hough transform (LM)')
    # # ax[0].set_axis_off()
    #
    # ax[1].imshow(np.log(1 + h))
    # ax[1].set_title('Hough transform (SKIImage)')
    # ax[1].set_xlabel('Angles (degrees)')
    # ax[1].set_ylabel('Distance (pixels)')
    # ax[1].axis('image')
    #
    # ax[2].imshow(image, origin='lower', aspect='equal', vmin=1999, vmax=2055)
    # origin = np.array((0, image.shape[1]))
    # c= 0
    # for _, angle, dist in zip(*hough_line_peaks(h, theta, d)):
    #     y0, y1 = (dist - origin * np.cos(angle)) / np.sin(angle)
    #     ax[2].plot(origin, (y0, y1), '-r')
    #     if c > 3:
    #         break
    #     c = c + 1
    # ax[2].set_xlim(origin)
    # ax[2].set_ylim((image.shape[0], 0))
    # ax[2].set_axis_off()
    # ax[2].set_title('Detected lines')

    for i in range(len(reg)):
        yi1 = yp[xi[i]]
        xi1 = xp[xi[i]]
        yi2 = yp[yi[i]]
        xi2 = xp[yi[i]]
        # ax[2].plot((xi1, xi2), (yi1, yi2), color='green')
        plt.plot((xi1, xi2), (yi1, yi2), color='orange')
        si = (yi2 - yi1) / (xi2 - xi1)
        slope.append(si)
        print('angle ', 180. * np.arctan(si) / np.pi)
        for j in range(i + 1, len(reg)):
            yj1 = yp[xi[j]]
            xj1 = xp[xi[j]]
            yj2 = yp[yi[j]]
            xj2 = xp[yi[j]]
            sj = (yj2 - yj1) / (xj2 - xj1)
            xicpt = (yi1 - yj1 - si * xi1 + sj * xj1) / (sj - si)
            yicpt = yi1 + si * (xicpt - xi1)
            isect.append((xicpt, yicpt))
            print('intersection ', df, xicpt, yicpt)
            # plt.plot(xicpt,yicpt,'o')

    plt.xlabel('x')
    plt.ylabel('y')
    plt.axis('equal')
    plt.xlim(((xlen // 2) - maxr, (xlen // 2) + maxr))
    plt.ylim(((xlen // 2) - maxr, (xlen // 2) + maxr))
    plt.tight_layout()
    plt.savefig('intersections.png')
    plt.close()

    return isect, slope


if __name__ == '__main__':

    # number of focus states to be tested
    nfocus = 1

    # number of intersections for nstrut=3 struts
    nstrut = 3
    nicpt = (nstrut * (nstrut - 1)) // 2

    # plt.figure()

    # initialise arrays
    meanx = np.zeros((nfocus, nicpt))
    meany = np.zeros((nfocus, nicpt))
    rmsx = np.zeros((nfocus, nicpt))
    rmsy = np.zeros((nfocus, nicpt))
    meanslopes = np.zeros((nfocus, nicpt))

    dirs = ('./resources',)

    # number of noise simulations to test - in this case set to unity as
    # we are just testing on one simulated image
    nsim = 1

    xplot = np.zeros((nfocus, nicpt, nsim))
    yplot = np.zeros((nfocus, nicpt, nsim))

    # test on various images in principle, here set up only to test on one image
    # this code assumes there is one star of interest and it is in the centre of the image
    for m, df in enumerate(dirs):

        # load the image
        # fname = os.path.join(df, 'extracted_image_3.fits')
        fname = os.path.join(df, 'extracted_image_no_cr.fits')
        fitsdata = load_fits(fname, 0)

        simslopes = []

        for sim in range(nsim):

            # put the image into a numoy array
            dsize = np.shape(fitsdata)
            image = np.zeros(dsize)
            image[...] = (fitsdata.astype(int)).astype(float)

            # find the Hough transform lines and their intersections
            isect, slopes = hough_transform(image, df, radius=1, maxr=100)

            # the output is the intercepts and slopes of the lines, append the slopes to a list
            simslopes.append(np.array(slopes))

            # work out separations between intersections 
            ivals = np.array(isect)
            n = 0
            for i in range(nicpt):
                ii = (i + 1) % nicpt
                xplot[m, n, sim] = ivals[i, 0] - np.mean(ivals[:, 0])
                yplot[m, n, sim] = ivals[i, 1] - np.mean(ivals[:, 1])
                n += 1

        simslopes = np.array(simslopes)
        meanslopes[m] = np.mean(simslopes, axis=0)

    # write out strut orientations
    beta = np.arctan(meanslopes[nfocus // 2])
    beta[1] += np.pi
    beta[2] += np.pi
    print('beta = ', beta * 180. / np.pi)
