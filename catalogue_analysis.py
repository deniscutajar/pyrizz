import astropy.io.fits as pf
from matplotlib import pyplot as plt

def plot_star_position(star_coord, pointing, label):
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))
    ax1.plot(star_coord['RA'], star_coord['DEC'], 'b.', label=label)

    ax1.plot(pointing[0], pointing[1], 'r+', label='pointing')

    ax1.set_xlabel('RA (deg)')
    ax1.set_ylabel('Dec. (deg)')
    ax1.legend()

def plot_magnitude(gaia_vis_data, besancon_vis_data):
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))
    num_bins = 50
    n, bins, patches = ax1.hist(gaia_vis_data, num_bins, facecolor='red', alpha=0.8, label='GAIA')
    n, bins, patches = ax1.hist(besancon_vis_data, num_bins, facecolor='blue', alpha=0.8, label='Besancon')

    ax1.set_xlabel('VIS Mag.')
    ax1.set_ylabel('Count')
    plt.legend()

def plot_sed_index(data1, data2, data3):
    x = data1['SED_TEMPLATE']
    num_bins = 25
    n, bins, patches = plt.hist(x, num_bins, facecolor='blue', alpha=0.5, label='gaia')

    x = data2['INDEX_SED']
    num_bins = 25
    n, bins, patches = plt.hist(x, num_bins, facecolor='red', alpha=0.5, label='Besancon')

    x = data3['SED_TEMPLATE']
    num_bins = 25
    n, bins, patches = plt.hist(x, num_bins, facecolor='green', alpha=0.5, label='Original Elvis')

    plt.legend()
    plt.show()



if __name__ == '__main__':
    ROOT = "/home/denis/Development/ELViS/work_dir/"
    elvis_star_catalog_filepath = ROOT + "ppos/PLAN-SIM_SC8_GSIR_SWF3_PPO_1_091120-PPO-0-201109-115132-639/data/star_subset_modified.fits"
    gaia_star_catalog_filepath = ROOT + "ppos/gaia_star_catalog_nep_1deg_sel.fits"
    besancon_star_catalog_filepath = "/home/denis/Development/ELViS/1.18/besancon_nep_cfhtls_star_catalog_FULL.fits"

    elvis_cat = pf.open(elvis_star_catalog_filepath, mode='denywrite')
    gaia_cat = pf.open(gaia_star_catalog_filepath, mode='denywrite')
    besancon_cat = pf.open(besancon_star_catalog_filepath, mode='denywrite')

    # plot_star_position(besancon_cat[1].data, pointing=[270.0, 66.5607], label='Besancon catalog')
    # plot_star_position(gaia_cat[1].data, pointing=[270.0, 66.5607], label='GAIA catalog')
    # plot_star_position(elvis_cat[1].data, pointing=[270.0, 66.5607], label='ELViS catalog')

    # plot_magnitude(gaia_cat[1].data['TU_FLUX_VIS'], elvis_cat[1].data['TU_FNU_VIS'])

    plot_sed_index(gaia_cat[1].data, besancon_cat[1].data, elvis_cat[1].data)


    elvis_cat.close()
    besancon_cat.close()
    gaia_cat.close()

    plt.show()