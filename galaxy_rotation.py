import astropy.io.fits as pf
import numpy as np
import os
from astropy import units as u
from astropy.coordinates import SkyCoord
from matplotlib import pyplot as plt
from astropy.units import Quantity


def analysis(data):
    median_ra = np.median(data['RA'])
    median_dec = np.median(data['DEC'])

    print(f"Transformed {len(data)} galaxy coordinates")
    print(f"Median RA: {median_ra:2.2f}, Dec: {median_dec:2.2f}")


def plot_coord(transformed_coord, original_coord=None):
    fig, (ax1, ax2) = plt.subplots(2, figsize=(11, 8))
    ax1.plot(transformed_coord['RA'], transformed_coord['DEC'], 'b.', label='Transformed')

    ax1.set_xlabel('RA (deg)')
    ax1.set_ylabel('Dec. (deg)')
    ax1.legend()

    if original_coord is not None:
        ax2.plot(transformed_coord['RA'], transformed_coord['DEC'], 'b.', label='Transformed')
        ax2.plot(original_coord['RA'], original_coord['DEC'], 'r.', label='Original')

    ax2.set_xlabel('RA (deg)')
    ax2.set_ylabel('Dec. (deg)')
    ax2.legend()
    plt.savefig('original_coordinates.png')


def plot_lon_lat(elon, elat):
    fig, ax = plt.subplots(1, figsize=(11, 8))
    # Ecliptic longitude

    ax.plot(elon, elat, 'r.')

    ax.set_xlabel('Lon (deg)')
    ax.set_ylabel('Lat (deg)')

    plt.savefig('lon_lat_distribution.png')


def nep_filter(data):
    """
    Return only rows near the NEP [270, 66.5607]
    """

    ra = 269.97
    dec = 66.56
    pad = 0.5
    min_dec = dec - pad
    max_dec = dec + pad
    min_ra = ra - 0.4/np.cos(np.deg2rad(dec))
    max_ra = ra + 0.4/np.cos(np.deg2rad(dec))

    return data[(data['RA'] <= max_ra) & (data['RA'] >= min_ra) & (data['DEC'] >= min_dec) & (data['DEC'] <= max_dec)]


if __name__ == '__main__':
    ROOT = "./resources/"

    # Read existing catalogue
    gc_filepath = ROOT + "EUC_SIM_TUGALCAT.fits"
    gc = pf.open(gc_filepath, mode='denywrite')

    # Read RA and Dec of existing galaxies
    org_dec = np.deg2rad(gc[1].data["DEC"])
    org_ra = np.deg2rad(gc[1].data["RA"])

    # New Pole should is at the centre of the existing distribution of galaxies
    new_pole_dec = np.median(org_dec)
    new_pole_ra = np.median(org_ra)

    # Determine the RA, Dec of the new galaxies in the rotated coordinate system
    d_alpha = org_ra - new_pole_ra
    sine_new_dec = np.sin(org_dec) * np.sin(new_pole_dec) + np.cos(org_dec) * np.cos(new_pole_dec) * np.cos(d_alpha)
    e_lat = np.arcsin(sine_new_dec)
    cosine_new_ra = (np.cos(org_dec) / np.cos(e_lat)) * np.sin(d_alpha)

    negative_ra_mask = np.sin(org_dec) < np.sin(new_pole_dec) * np.sin(e_lat)

    # Adjust the result of the ecliptic longitude
    e_long = np.arccos(cosine_new_ra)
    e_long[negative_ra_mask] = 2 * np.pi - np.arccos(cosine_new_ra[negative_ra_mask])

    # Save the catalogue to disk
    gc_filepath = os.path.splitext(gc_filepath)[0] + "_NEW.fits"
    ngc = pf.open(gc_filepath, mode='update')

    e_lat_deg = np.rad2deg(e_lat)
    e_long_deg = np.rad2deg(e_long)

    plot_lon_lat(e_long_deg, e_lat_deg)

    ecl_coord = SkyCoord(e_long_deg * u.degree, e_lat_deg * u.degree, frame='barycentricmeanecliptic')
    eq_coord = ecl_coord.transform_to('icrs')

    ngc[1].data["DEC"] = eq_coord.dec.degree
    ngc[1].data["RA"] = eq_coord.ra.degree

    ngc[1].data = nep_filter(ngc[1].data)

    # remove nan from the dataset
    ngc[1].data = ngc[1].data[~np.isnan(ngc[1].data["RA"])]

    ngc.flush()

    analysis(ngc[1].data)
    plot_coord(ngc[1].data, ngc[1].data)

    plt.show()

    gc.close()
    ngc.close()
