import astropy.io.fits as pf
from matplotlib import pyplot as plt

def plot_star_position(star_coord, pointing):
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))
    ax1.plot(star_coord['RA'], star_coord['DEC'], 'b.', label='GAIA Stars')

    ax1.plot(pointing[0], pointing[1], 'r+', label='GAIA pointing')

    ax1.set_xlabel('RA (deg)')
    ax1.set_ylabel('Dec. (deg)')
    ax1.legend()

    plt.show()

def plot_magnitude_dist(flux):
    num_bins = 250
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))

    # mag = flux2mag(flux)

    n, bins, patches = plt.hist(flux, num_bins, facecolor='green', alpha=0.5, label='tu_fnu_vis_mag')

    plt.show()


if __name__ == '__main__':
    ROOT = "/home/denis/Development/ELViS/work_dir/"
    spectra_file = ROOT + "ppos/PLAN-SIM_SC8_GSIR_SWF3_PPO_1_091120-PPO-0-201109-115132-639/data/EUC-TEST-SEDLIB-2013-11-14_MODIF.fits"
    star_file = ROOT + "ppos/PLAN-SIM_SC8_GSIR_SWF3_PPO_1_091120-PPO-0-201109-115132-639/data/EUC_SIM_TUSTARCAT-37729_0BC97E1E92FA-0041495_20210216T132528.457005Z_v13_SC8_MAIN.fits"
    star_file = ROOT + "ppos/PLAN-SIM_SC8_GSIR_SWF3_PPO_1_091120-PPO-0-201109-115132-639/data/star_subset_modified.fits"
    star_file = ROOT + "ppos/gaia_star_catalog_nep_1deg_sel.fits"

    star_cat = pf.open(star_file, mode='denywrite')

    # plot_star_position(star_cat[1].data, pointing=[270.0, 66.5607])

    plot_magnitude_dist(star_cat[1].data['TU_FLUX_VIS'])

    star_cat.close()
