import astropy.io.fits as pf
import astropy.units as u
import numpy as np
from astropy.coordinates import SkyCoord
from astropy.table import Table
from astropy.units import Quantity
from astroquery.gaia import Gaia
from matplotlib import pyplot as plt
import swig_mw_extinction
import numpy as np
import astropy.units as u
import astropy.io.fits as pf
from Assign_Gaia_SED import get_mags, get_bandpasses, readSED, colour_match
from av_calc import read_catalog, get_mwext, calculate_av, get_av
import os
import numpy.lib.recfunctions as rfn
import time

def dump_catalog_elvis(catalogue: Table, filename: str) -> object:
    """
    Write catalogue to an ELViS format

    :param catalog:
    :type catalog:
    :param filename:
    :type filename:
    :return:
    :rtype:
    """
    #

    # mean_av = np.nanmean(catalogue['a_g_val'])
    # catalogue['a_g_val'][catalogue['a_g_val'].mask] = mean_av

    # if LIMIT:
    #     catalogue = catalogue[: LIMIT]

    nstars = len(catalogue)

    cols = [pf.Column(name='RA', format='D', array=catalogue['ra']),
            pf.Column(name='DEC', format='D', array=catalogue['dec']),
            # AV: StellarExtinction, a_g_val: line-of-sight extinction in the G band, A_G (float, Magnitude[mag])
            pf.Column(name='AV', format='D', array=catalogue['AV']),
            # DIST: Star Distance in kpc
            pf.Column(name='DIST', format='D', array=catalogue['dist']),
            pf.Column(name='G_MAG', format='D', array=catalogue['gmag']),
            pf.Column(name='BP_MAG', format='D', array=catalogue['phot_bp_mean_mag']),
            pf.Column(name='RP_MAG', format='D', array=catalogue['phot_rp_mean_mag']),
            pf.Column(name='TU_FLUX_VIS', format='D', array=catalogue['vismag']),
            pf.Column(name='H_MAG', format='D', array=catalogue['Hmag']),
            pf.Column(name='G_TEFF', format='D', array=catalogue['gteff']),
            pf.Column(name='Matched_TEFF', format='D', array=catalogue['matched_teff']),
            pf.Column(name='SIZE_ARCSEC', format='E', array=np.ones((nstars)) * 3.0),
            pf.Column(name='SED_TEMPLATE', format='J', array=catalogue['SED_TEMPLATE']),
            ]

    fd = pf.HDUList()
    fd.append(pf.PrimaryHDU())

    bintable = pf.BinTableHDU.from_columns(cols)
    fd.append(bintable)
    fd.writeto(filename, overwrite=True)

    print(f"Star catalog written to {filename}")

    # visualise_catalogue(bintable.data, POINTING, RADIUS)

    return filename


def sed_match(bandpasses, gdata, wlens, params, seds):
    # read the catalogue of SEDs
    # adjust path as necessary!

    nspec, nlambda = seds.shape

    # get the spectrum parameters that were read from the SEDs file
    logg = params.field(0)
    teff = params.field(1)
    feh = params.field(2)

    # select dwarf stars (high gravity), with reasonable feh metallicity, and not too hot from the SED table
    starsel = (logg > 3.5) & (feh > -1) & (teff < 12000)
    # remember the selected indices in the original array
    sedindex = np.arange(nspec, dtype=int)
    sedindex = sedindex[starsel]

    # create the VIS, Gaia bandpasses and zeropoint definitions
    bandlist, zplist, refmag = bandpasses

    # get the magnitudes in each band of the selected template SEDs
    sed_mags = get_mags(seds[starsel], wlens, bandlist, zplist, refmag)

    # get Gaia data from a pre-prepared file at the NEP
    # for the polar cap at elat>80 degrees
    # gaia_fits_table = 'ecl_80_91-result.fits.gz'
    # gdata = read_gaia_fits(gaia_fits_table)
    #
    # elon = gdata['ecl_lon']
    # elat = gdata['ecl_lat']
    rpmag = gdata['phot_rp_mean_mag']
    bpmag = gdata['phot_bp_mean_mag']
    gmag = gdata['phot_g_mean_mag']
    gteff = gdata['teff_val']
    #
    # # select Gaia stars near the NEP
    # # (change this to match the stars selected in an actual VIS field)
    # sel = elat >= elat_thres
    #
    # # only keep catalogue values for these NEP-selected stars
    # elon = elon[sel]
    # elat = elat[sel]
    # rpmag = rpmag[sel]
    # bpmag = bpmag[sel]
    # gmag = gmag[sel]
    # gteff = gteff[sel]

    # package selected magnitudes into array
    # observed_mags = np.zeros([len(rpmag), 3])
    observed_mags = np.zeros([1, 3])
    observed_mags[:, 0] = bpmag
    observed_mags[:, 1] = rpmag
    observed_mags[:, 2] = gmag

    # identify gaia stars with unmeasured colours
    no_colours = np.isnan(rpmag) | np.isnan(bpmag)

    # for those stars without measured colours, find the median colour at faint magnitudes and use that instead
    sel = (gmag > 18.) & ~no_colours
    gbp = np.median(gmag[sel] - bpmag[sel])
    grp = np.median(gmag[sel] - rpmag[sel])
    observed_mags[no_colours, 0] = observed_mags[no_colours, 2] - gbp
    observed_mags[no_colours, 1] = observed_mags[no_colours, 2] - grp

    # filter for valid temperature values in the gaia data (only used for the final comparison test)
    # note only bright stars have Gaia temperatures
    gaia_temp_exists = ~np.isnan(gteff)
    gteff[np.isnan(gteff)] = -99

    # match Gaia star colours to SED colours
    # (strip the VIS,H magnitudes off the SED mags for the colour matching, as VIS,H not measured by gaia)
    gaia_match_index, minvar = colour_match(observed_mags, sed_mags[:, 1:4])

    # match this index back to the index in the original SED table
    # (remember that we excluded some SEDs by the selection criteria)
    sedmatch = sedindex[gaia_match_index]

    # find the SED temperature for the matches
    matched_teff = teff[sedmatch]

    # find the VIS magnitude from the best matching SED
    # this uses the selected sed_mags array
    # and calculates  vismag = sed_mag(VIS band) + ( Gaia G mag ) - sed_mag(G band)
    vismag = sed_mags[gaia_match_index, 0] + gmag - sed_mags[gaia_match_index, 3]

    # likewise for the H band
    Hmag = sed_mags[gaia_match_index, 4] + gmag - sed_mags[gaia_match_index, 3]

    # cols = np.column_stack((elon, elat, rpmag, bpmag, gmag, vismag, Hmag, gteff, matched_teff, sedmatch))

    # Output the Gaia values, VIS magnitude, gaia and fitted temperatures, matching index
    gdata['gmag'] = gmag
    gdata['vismag'] = vismag
    gdata['Hmag'] = Hmag
    gdata['gteff'] = gteff
    gdata['matched_teff'] = matched_teff
    gdata['SED_TEMPLATE'] = sedmatch

    return gdata


def visualise_catalogue(star_coord, pointing, radius):
    fig, (ax1) = plt.subplots(1, figsize=(11, 8))
    ax1.plot(star_coord['ra'], star_coord['dec'], 'b.', label='GAIA Stars')

    ax1.plot(pointing[0], pointing[1], 'r+', label='GAIA pointing')
    circle = plt.Circle((pointing[0], pointing[1]), radius.value, color='r', fill=False)

    ax1.add_patch(circle)

    ax1.set_xlabel('RA (deg)')
    ax1.set_ylabel('Dec. (deg)')
    ax1.legend()

    plt.show()


def av_match(av, avs):
    return np.abs(avs - av).argmin()


def e_array(size, type=None):
    if type is None:
        return np.zeros(size)

    return np.zeros(size, dtype=type)


if __name__ == '__main__':
    CACHE = True
    CACHE_FILEPATH = 'gaia_stars.npy'
    ELViS = True
    OUTPUT_ROOT = '/home/denis/Development/ELViS/work_dir/ppos/'
    LIMIT = None  # number of stars to download
    POINTING = [270, 66.5607]  # area near the North Ecliptic Cap.
    RADIUS = Quantity(0.8, u.degree)
    SED_CATALOGUE = 'EUC-TEST-SEDLIB-2013-11-14_MODIF.fits'
    SCHLEGEL_NORTH_FILEPATH = 'SFD_dust_4096_ngp.fits'  # https://github.com/kbarbary/sfddata
    SCHLEGEL_SOUTH_FILEPATH = 'SFD_dust_4096_sgp.fits'  # https://github.com/kbarbary/sfddata
    OUTPUT_AV_SED_MAP = 'EUC-TEST-SEDLIB-2013-11-14_MODIF_REDDENING.npy'
    OUTPUT_AV_SEDMAG_MAP = 'EUC-TEST-SEDLIB-2013-11-14_MODIF_SEDMAG.npy'
    AV_start = 0.100
    AV_stop = 0.160
    AV_step = 0.001
    AV_SAMPLING_RANGE = np.arange(start=AV_start, stop=AV_stop, step=AV_step)

    log_file = "gaia_downloader__log_file.txt"
    output_file = "gaia_downloader__input_stars"
    Gaia.ROW_LIMIT = 10000

    # So our procedure should be:
    # Get list of stars (from gaia)
    # Deduce the Av value for a star
    # Apply the reddening to all the SEDs using the sim function
    # Match the SED and get the H band magnitude
    # Repeat for all stars (as they have different Av values)

    file_name = 'gaia_star_catalog_nep_1deg_sel.fits'
    if LIMIT is not None:
        file_name = 'gaia_star_catalog_nep_1deg_small.fits'

    # Create a reference coordinate
    coord = SkyCoord(ra=POINTING[0], dec=POINTING[1], unit=(u.degree, u.degree), frame='icrs')

    # Retrieve the stars around the reference coordinate
    # Get Gaia data from a pre-prepared file at the NEP
    if CACHE and os.path.isfile(CACHE_FILEPATH):
        gdata = np.load(CACHE_FILEPATH, allow_pickle=True)

        if LIMIT:
            gdata = gdata[:LIMIT]
    else:
        gdata = Gaia.query_object_async(coordinate=coord, radius=RADIUS)
        np.save(CACHE_FILEPATH, gdata)

    # Load the MWEXT extension
    mwext = get_mwext(SCHLEGEL_NORTH_FILEPATH, SCHLEGEL_SOUTH_FILEPATH)

    size = len(gdata)

    e_array(size, type=int)
    # Deduce the Av value for each star
    gdata = rfn.append_fields(gdata, ['AV', 'AV_map', 'gmag', 'vismag', 'Hmag', 'gteff', 'matched_teff', 'SED_TEMPLATE'],
                              [calculate_av(mwext, gdata), e_array(size, type=int), e_array(size), e_array(size),
                               e_array(size), e_array(size), e_array(size), e_array(size, type=int)])

    av_sed_map = np.load(OUTPUT_AV_SED_MAP)
    av_sed_mag_map = np.load(OUTPUT_AV_SEDMAG_MAP)
    wlens, _, params = readSED(SED_CATALOGUE)

    # get the spectrum parameters that were read from the SEDs file
    logg = params.field(0)
    teff = params.field(1)
    feh = params.field(2)

    rpmag = gdata['phot_rp_mean_mag']
    bpmag = gdata['phot_bp_mean_mag']
    gmag = gdata['phot_g_mean_mag']
    gteff = gdata['teff_val']

    # package selected magnitudes into array
    observed_mags = np.zeros([len(rpmag), 3])
    observed_mags[:, 0] = bpmag
    observed_mags[:, 1] = rpmag
    observed_mags[:, 2] = gmag

    # identify gaia stars with unmeasured colours
    no_colours = np.isnan(rpmag) | np.isnan(bpmag)

    # for those stars without measured colours, find the median colour at faint magnitudes and use that instead
    sel = (gmag > 18.) & ~no_colours
    gbp = np.median(gmag[sel] - bpmag[sel])
    grp = np.median(gmag[sel] - rpmag[sel])
    observed_mags[no_colours, 0] = observed_mags[no_colours, 2] - gbp
    observed_mags[no_colours, 1] = observed_mags[no_colours, 2] - grp

    # filter for valid temperature values in the gaia data (only used for the final comparison test)
    # note only bright stars have Gaia temperatures
    gaia_temp_exists = ~np.isnan(gteff)
    gteff[np.isnan(gteff)] = -99

    # create the VIS, Gaia bandpasses and zeropoint definitions
    bandlist, zplist, refmag = get_bandpasses()

    # select dwarf stars (high gravity), with reasonable feh metallicity, and not too hot from the SED table
    starsel = (logg > 3.5) & (feh > -1) & (teff < 12000)

    for _, star in enumerate(gdata):
        star['AV_map'] = av_match(star['AV'], AV_SAMPLING_RANGE)

    t1 = time.time()
    sedindex = None
    for i, AV_index in enumerate(np.unique(gdata['AV_map'])):
        mask = gdata['AV_map'] == AV_index
        stargroup = gdata[mask]
        seds = av_sed_map[AV_index]
        sed_mags = av_sed_mag_map[AV_index][starsel]
        if sedindex is None:
            nspec, nlambda = seds.shape

            # remember the selected indices in the original array
            sedindex = np.arange(nspec, dtype=int)
            sedindex = sedindex[starsel] # had to be commented out since gaia index was throwing and index error


        # match Gaia star colours to SED colours
        # (strip the VIS,H magnitudes off the SED mags for the colour matching, as VIS,H not measured by gaia)
        # gaia_match_index, minvar = colour_match(np.array(observed_mags[i, :]), sed_mags[:, 1:4])
        gaia_match_index, minvar = colour_match(observed_mags[mask], sed_mags[:, 1:4])

        # match this index back to the index in the original SED table
        # (remember that we excluded some SEDs by the selection criteria)
        sedmatch = sedindex[gaia_match_index]

        # find the SED temperature for the matches
        matched_teff = teff[sedmatch]

        # find the VIS magnitude from the best matching SED
        # this uses the selected sed_mags array
        # and calculates vismag = sed_mag(VIS band) + ( Gaia G mag ) - sed_mag(G band)
        vismag = sed_mags[gaia_match_index, 0] + gmag[mask] - sed_mags[gaia_match_index, 3]

        # likewise for the H band
        Hmag = sed_mags[gaia_match_index, 4] + gmag[mask] - sed_mags[gaia_match_index, 3]

        # Output the Gaia values, VIS magnitude, gaia and fitted temperatures, matching index
        stargroup['gmag'] = gdata[mask]['phot_g_mean_mag']
        stargroup['vismag'] = vismag
        stargroup['Hmag'] = Hmag
        stargroup['gteff'] = gdata[mask]['teff_val']
        stargroup['matched_teff'] = matched_teff
        stargroup['SED_TEMPLATE'] = sedmatch

        gdata[mask] = stargroup
    print(f"{len(gdata)} stars processed in {time.time() - t1: 2.3f} seconds")
    dump_catalog_elvis(gdata, OUTPUT_ROOT + file_name)
